<!DOCTYPE html>
<html>
<head>
    <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
</head>
<body>
<div class="container">
    <textarea class="input-field" cols="100" rows="5">lol lele lol</textarea>
    <div class="result"></div>
</div>
<script>
    $('.container .input-field').on('input', function () {
        var text = $(this).val();
        console.log(text);
        if(text !== undefined){

            var words = text.split(new RegExp('[. ,\n/:?]+', 'g'));

            //var words = text.split(/(?:,| |.)+/);
            words = words.map(word => word.trim());

            var words_length = words.length;

            var word_repetition_rate = [];
            for (var i = 0; i < words_length; i++)
            {
                if(word_repetition_rate[words[i]] === undefined){
                    word_repetition_rate[words[i]] = 1;
                }else{
                    word_repetition_rate[words[i]] = word_repetition_rate[words[i]] + 1;
                }
            }

            var words_bold = [];
            for(word in word_repetition_rate) {
                if(word_repetition_rate[word] > 1){
                    words_bold.push(word);
                }
            }

            for(index in words_bold) {
                console.log(words_bold[index]);

                console.log('<b>'+ words_bold[index] + '</b>');
                text = text.split(words_bold[index]).join('<b>'+ words_bold[index] + '</b>');
            }

            $(".result").html(text);
        }
    });
</script>
</body>
</html>