<?php
namespace Notes;
class Note
{
    private $pdo;

    public function __construct($pdo){
        $this->pdo = $pdo;
    }

    public function add($text){
        $query = "INSERT INTO mipt_note (text) VALUES (:text)";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute(["text" => $text]);
    }

    public function delete($id){
        $query = "DELETE FROM mipt_note WHERE id = :id";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute(["id" => $id]);
    }

    public function getAll(){
        $query = "SELECT * FROM mipt_note ORDER BY id DESC";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute();
        $response = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $response;
    }
}