<?php
namespace Notes;
class DB{
    private static $pdo = null;

    public static function init($config_path)
    {
        if (static::$pdo === null) {
            $confArray = require("conf/" .$config_path);

            self::$pdo = new \PDO(
                'pgsql:host=' . $confArray['db.host'] . ';dbname=' . $confArray['db.basename'] . ';user=' . $confArray['db.user'] . ';password=' . $confArray['db.password']
            );
            self::$pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            self::$pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
            self::$pdo->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
        }

        return self::$pdo;
    }
}