<?php
namespace Notes;
class View
{
    public function render($templateFilePath, array $data = array())
    {
        ob_start();
        extract($data, EXTR_OVERWRITE);
        require $templateFilePath;
        return ob_get_clean();
    }
}