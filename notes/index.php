<?php
require $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';

$pdo = \Notes\DB::init("conf.db.php");
$note = new \Notes\Note($pdo);

$data = $note->getAll();

$view = new \Notes\View();
$html = $view->render("templates/main.php", $data);

echo $html;