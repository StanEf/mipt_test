<!DOCTYPE html>
<html>
<head>
    <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
    <link rel="stylesheet/less" type="text/css" href="styles.less" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/3.9.0/less.min.js" ></script>
</head>
<body>
<div class="main">
    <div class="block-add">
        <div class="block-add-input-field">
            <input type="text" class="input-field">
        </div>
        <div class="block-add-button">
            <input type="button" value="Добавить">
        </div>
    </div>
    <div class="block-show-all">

        <? foreach($data as $row): ?>
        <div class="note">
            <div class="note-text">
                <?= htmlspecialchars($row["text"], ENT_QUOTES | ENT_SUBSTITUTE) ?>
            </div>
            <div class="note-delete-button">
                <input type="button" value="Удалить" data-note-id="<?= $row["id"] ?>">
            </div>
        </div>
        <? endforeach; ?>

    </div>
</div>
</body>
<script>
    $(".block-add-button input").on("click", function(){
        let data = {};
        data["text"] = $(".block-add-input-field .input-field").val();
        data["action"] = "add";
        $.ajax({
            url: "ajax.php",
            type: "POST",
            data: data,
        }).done(function() {
            location.reload();
        });
    });

    $(".block-show-all .note-delete-button input").on("click", function(){
        let data = {};
        data["id"] = $(this).data("note-id");
        data["action"] = "delete";
        $.ajax({
            url: "ajax.php",
            type: "POST",
            data: data,
        }).done(function() {
            location.reload();
        });
    });
</script>

</html>
