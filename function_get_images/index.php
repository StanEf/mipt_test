<?
function getImages($path) {
    $files = scandir($path);
    $images = [];
    foreach($files as $key => $file){
        if($key == 0 || $key == 1){
            continue;
        }
        
        $file_full_path = $path . "/" . $file;

        if(filesize($file_full_path) > 11 && exif_imagetype($file_full_path)){
            $images[] = $file;
        }
    }

    $image_filtered = [];
    $filter_size = 5*1024*1024;

    if(!empty($images)){
        foreach($images as $image){
            if(filesize($path . "/" . $image) >= $filter_size){
                $image_filtered[] = $image;
            }
        }
    }
    
    return $image_filtered;
}

$images_more_then_5_mb = getImages("test_directory");
echo '<br/> $images_more_then_5_mb '. __LINE__.'* ' .  __FILE__ . ' <pre style="text-align: left;">';
print_r($images_more_then_5_mb);
echo '</pre>';